datalife_phyto_stock
====================

The phyto_stock module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-phyto_stock/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-phyto_stock)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
