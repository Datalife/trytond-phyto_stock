# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest

import doctest

from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite
from trytond.tests.test_tryton import doctest_checker, doctest_teardown


class PhytoStockTestCase(ModuleTestCase):
    """Test Phyto Stock module"""
    module = 'phyto_stock'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            PhytoStockTestCase))
    suite.addTests(doctest.DocFileSuite(
            'scenario_phyto_stock.rst',
            tearDown=doctest_teardown, encoding='utf-8',
            checker=doctest_checker,
            optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    return suite
