# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, fields
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.wizard import Button
from trytond.wizard import StateReport
from trytond.wizard import StateView
from trytond.wizard import Wizard


class MoveReport(Report):
    """Phyto Move Report"""
    __name__ = 'phyto.move_report'

    @classmethod
    def get_context(cls, records, header, data):
        pool = Pool()
        Company = pool.get('company.company')

        context = super().get_context(records, header, data)
        context['company'] = Company(Transaction().context.get('company'))
        return context


class MoveReportPrint(Wizard):
    """Phyto Move Report Print"""
    __name__ = 'phyto.move_report.print'

    start = StateView(
        'phyto.move_report.print.start',
        'phyto_stock.report_print_start_view_form',
        [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])

    print_ = StateReport('phyto.move_report')

    def do_print_(self, action):
        pool = Pool()
        Move = pool.get('stock.move')

        moves = Move.search(self._get_move_domain())

        return action, {'ids': [m.id for m in moves]}

    def _get_move_domain(self):
        Modeldata = Pool().get('ir.model.data')
        pytho_id = Modeldata.get_id('phyto', 'phyto_product_phytosanitary')

        locations = []
        for wh in self.start.warehouses:
            locations.extend(list({wh.storage_location, wh.input_location,
                wh.output_location}))
        return [
            ('state', '!=', 'cancelled'),
            ('effective_date', '>=', self.start.initial_date),
            ('effective_date', '<=', self.start.end_date),
            ['OR',
                [
                    ('from_location', 'child_of', locations, 'parent'),
                    ('to_location.type', 'in', ('supplier', 'customer'))
                ],
                [
                    ('to_location', 'child_of', locations, 'parent'),
                    ('from_location.type', 'in', ('supplier', 'customer'))
                ]
            ],
            ('product.categories', 'child_of', [pytho_id], 'parent'),
            ('shipment', '!=', None)
        ]


class MoveReportPrintStart(ModelView):
    """Report Print Start"""
    __name__ = 'phyto.move_report.print.start'

    initial_date = fields.Date('Initial Date', required=True)
    end_date = fields.Date('End Date', required=True)
    warehouses = fields.Many2Many('stock.location', None, None,
        'Warehouses', required=True,
        domain=[('type', '=', 'warehouse')])
